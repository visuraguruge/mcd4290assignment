// The class related to control the booking of a trip.

class tripBooking
{
constructor()
{

this._pickUpTime = " ";
this._dropOffPoint =" ";
this._pickUpPoint =" ";
this._stopsMade =[];
this._distanceTravelled=" ";

}

    get pickUpTime(){ return this._pickUpTime; }
    get dropOffPoint(){ return this._dropOffPoint; }
    get pickUpPoint(){ return this._pickUpPoint; }
    get stopsMade(){ return this._stopsMade; }
    get distanceTravelled(){ return this._distanceTravelled }
	
	set pickUpTime(pickUpTime ){this._pickUpTime =pickUpTime ; }
    set dropOffPoint(dropOffPoint){this._dropOffPoint=dropOffPoint; }
    set pickUpPoint(pickUpPoint) {this._pickUpPoint=pickUpPoint; }
    set stopsMade(stopsMade){this._stopsMade=stopsMade; }
    set distanceTravelled(distanceTravelled){this._distanceTravelled=distanceTravelled}
	
// This will confirm the trip when the confirm button is pressed.		
	confirmTrip(){}
	
// This will cancel the trip & reset the trip info as the user presses the cancel button.	
	cancelTrip(){
	
		this._pickUpTime = " ";
        this._dropOffPoint = " ";
        this._pickUpPoint =" ";
        this._stopsMade = " ";
        this._distanceTravelled =[];
		
	}
	
	
}

// The class related to routes of the trip.

class routes(){
	
	constructor(){
	
	this._pickUp =" ";
    this._dropOff=" ";
    this._taxiType=" ";
	this._costOfTheRide=" ";

}

get pickUp(){return this._pickUp;}
get dropOff(){return this._dropOff;}
get taxiType(){return this._taxiType;}
get costOfTheRide(){return this._costOfTheRide;}

set pickUp(){return this._pickUp;}
set dropOff(){return this._dropOff;}
set taxiType(){return this._taxiType;}
set costOfTheRide(){return this._costOfTheRide;}

}